# Description 
#
# Given a list of people with their birth and end years (all between 1900 and 
# 2000), find the year with the most number of people alive.
#
# Code 
#
# Solve using a language of your choice and dataset of your own creation.



# Function that calculates the year with the most people alive, given a file 
# with birth and death data.
# Input: Path to file that contains birth and death records.
#        It is assumed that the file contains, on each line, a year of birth
#        and a year of death, separated by a whitespace. Lines that do not 
#        have exactly two entries are discarded.
# Returns: The year with the most people alive, and the number of people alive
#          that year.
# NOTE: There might be multiple years with an equal "maximum" number of people
#       alive. Since the problem description does not specify it, this function
#       returns only the first such year. 
def getMaxPeopleAlive(filename):
    inputFile = open(filename)

    startYear = 1900
    endYear = 2000
    # For array indexing purposes, the start year needs to be 0
    offset = startYear
    
    # Total count of people alive for each year
    peopleAlive = [0 for i in range(0, endYear - startYear + 1)]
        
    # Change in the number of people alive for each year
    peopleAliveDelta = [0 for i in range(0, endYear - startYear + 1)]
    
    for line in inputFile:
        data = line.split()
        if (len(data) == 2): #else this line is skipped
            # Assume entries are integers between 1900 and 2000
            birth = int(data[0])
            death = int(data[1])
            peopleAliveDelta[birth - offset] += 1
            peopleAliveDelta[death - offset] -= 1

    # People alive initially = births - deaths in the first year
    peopleAlive[0] = peopleAliveDelta[0]   

    # People alive in all other years = people alive in the previous years + births - deaths in the current year
    for year in range(startYear + 1, endYear + 1):
        peopleAlive[year - offset] += peopleAlive[year - offset - 1] + peopleAliveDelta[year - offset]

    maxPeopleAlive = max(peopleAlive)
    yearMaxPeopleAlive = offset + peopleAlive.index(maxPeopleAlive)
    # Return the maximum number of people alive, and the year in which this maximum occurred
    return yearMaxPeopleAlive, maxPeopleAlive


###############################################################################
# The sample data file contains ONLY people who were born and died between 1900
# and 2000, in accordance with the problem description
(year, maxPeopleAlive) = getMaxPeopleAlive('people.dat')
print ("There were %d people alive in %d" % (maxPeopleAlive, year))
        

